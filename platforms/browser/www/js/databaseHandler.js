var databaseHandler = {
db: null,
createDatabase: function(){
    this.db = window.openDatabase(
        "storages.db",
        "1.0",
        "storage database",
        10000000);
    this.db.transaction(
        function(tx){
            //Run sql here using tx
            tx.executeSql(
                `create table if not exists storages(_id integer primary key, 
                    storage_type text, 
                    dimension text, 
                    created_time text,
                    storage_feature text,
                    price text, 
                    notes text,
                    reporter_name text)`,
                [],
                function(tx, results){},
                function(tx, error){
                    console.log("Error while creating the table: " + error.message);
                }
            );
        },
        function(error){
            console.log("Transaction error: " + error.message);
        },
        function(){
            console.log("Create DB transaction completed successfully");
        }
    );

}
}
var storageHandler = {
    addStorage: function (storage_type, dimension, created_time, storage_feature, price, notes, reporter_name) {
        databaseHandler.db.transaction(
            function (tx) {
                tx.executeSql(
                    `insert into storages(storage_type, 
                    dimension, 
                    created_time, 
                    storage_feature,
                    price,
                    notes,
                    reporter_name) values(?, ?, ?, ?, ?, ?, ?)`,
                    [storage_type, dimension, created_time, storage_feature, price, notes, reporter_name],
                    function (tx, results) { },
                    function (tx, error) {
                        console.log("add storage error: " + error.message);
                    }
                );
            },
            function (error) { },
            function () { }
        );
    },
    loadStorages: function (displayStorages) {
        databaseHandler.db.readTransaction(
            function (tx) {
                tx.executeSql(
                    "select * from storages",
                    [],
                    function (tx, results) {
                        //Do the display
                        displayStorages(results);
                    },
                    function (tx, error) {//TODO: Alert the message to user
                        console.log("Error while selecting the storages" + error.message);
                    }
                );
            }
        );
    },
    deleteStorage: function (_id) {
        databaseHandler.db.transaction(
            function (tx) {
                tx.executeSql(
                    "delete from storages where _id = ?",
                    [_id],
                    function (tx, results) { },
                    function (tx, error) {//TODO: Could make an alert for this one.
                        console.log("Error happen when deleting: " + error.message);
                    }
                );
            }
        );
    },
    updateStorage: function (_id, storage_type, dimension, storage_feature, price, notes, reporter_name) {
        databaseHandler.db.transaction(
            function (tx) {
                tx.executeSql(
                    `update storages set storage_type=?, 
                    dimension=?,
                    storage_feature=?,
                    price=?,
                    notes=?,
                    reporter_name=? 
                where _id = ?`,
                    [storage_type, dimension, storage_feature, price, notes, reporter_name, _id],
                    function (tx, result) { },
                    function (tx, error) {//TODO: alert/display this message to user
                        console.log("Error updating storage" + error.message);
                    }
                );
            }
        );
    }
};
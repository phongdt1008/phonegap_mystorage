$(document).on("ready", function () {
    databaseHandler.createDatabase();
});
function addStorage() {
    var storage_type = $("#txtStorage_type").val();
    var dimension = $("#txtDimension").val();
    var storage_feature = $("#txtStorage_feature").val();
    var price = $("#txtPrice").val();
    var notes = $("#txtNotes").val();
    var reporter_name = $("#txtReporter_name").val();

    var today = new Date();
    var date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var created_time = date + ' ' + time;

    if (!storage_type) {
        alert("Storage type is required");
    }
    else if (!dimension) {
        alert("Dimension is required");
    }
    else if (!storage_feature) {
        alert("Storage feature is required");
    }
    else if (!price) {
        alert("Price is required");
    }
    else if (!reporter_name) {
        alert("Reporter name is required");
    }
    else {
        var r = confirm("Storage type: " + storage_type + "\n"
            + "Dimension: " + dimension + "\n"
            + "Storage feature: " + storage_feature + "\n"
            + "Price: " + price + "\n"
            + "Reporter name: " + reporter_name + "\n");
        if (r == true) {
            storageHandler.addStorage(storage_type
                , dimension
                , created_time
                , storage_feature
                , price
                , notes
                , reporter_name);
            $("#txtStorage_type").val("");
            $("#txtDimension").val("");
            $("#txtPrice").val("");
            $("#txtNotes").val("");
            $("#txtReporter_name").val("");
        }
    }
}
var currentStorage = {
    id: -1,
    storage_type: "",
    dimension: "",
    created_time: "",
    storage_feature: "",
    price: "",
    notes: "",
    reporter_name: "",
}
function displayStorages(results) {
    var length = results.rows.length;
    var lstStorages = $("#lstStorages");
    lstStorages.empty();//Clean the old data before adding.
    for (var i = 0; i < length; i++) {
        var item = results.rows.item(i);
        var a = $("<a />");
        var storage_type = $("<h3 />").text("Storage type: ");
        var dimension = $("<h4 />").text("Dimension: ");
        var created_time = $("<h4 />").text("Created time: ");
        var storage_feature = $("<h4 />").text("Storage feature: ");
        var price = $("<h4 />").text("Price: ");
        var notes = $("<h4 />").text("Notes: ");
        var reporter_name = $("<h4 />").text("Reporter name: ");
        var p = $("<p />").text("Id: ");

        var spanStorage_type = $("<span />").text(item.storage_type);
        spanStorage_type.attr("name", "storage_type");
        var spandDimension = $("<span />").text(item.dimension);
        spandDimension.attr("name", "dimension");
        var spandCreated_time = $("<span />").text(item.created_time);
        spandCreated_time.attr("name", "created_time");
        var spandStorage_feature = $("<span />").text(item.storage_feature);
        spandStorage_feature.attr("name", "storage_feature");
        var spandPrice = $("<span />").text(item.price);
        spandPrice.attr("name", "price");
        var spandNotes = $("<span />").text(item.notes);
        spandNotes.attr("name", "notes");
        var spandReporter_name = $("<span />").text(item.reporter_name);
        spandReporter_name.attr("name", "reporter_name");
        var spanId = $("<span />").text(item._id);
        spanId.attr("name", "_id");

        storage_type.append(spanStorage_type);
        dimension.append(spandDimension);
        created_time.append(spandCreated_time);
        storage_feature.append(spandStorage_feature);
        price.append(spandPrice);
        notes.append(spandNotes);
        reporter_name.append(spandReporter_name);
        p.append(spanId);

        a.append(storage_type);
        a.append(dimension);
        a.append(created_time);
        a.append(storage_feature);
        a.append(price);
        a.append(notes);
        a.append(reporter_name);
        a.append(p);

        var li = $("<li/>");
        li.attr("data-filtertext", item.name);
        li.append(a);
        lstStorages.append(li);
    }
    lstStorages.listview("refresh");
    lstStorages.on("tap", "li", function () {
        currentStorage.id = $(this).find("[name='_id']").text();
        currentStorage.storage_type = $(this).find("[name='storage_type']").text();
        currentStorage.dimension = $(this).find("[name='dimension']").text();
        currentStorage.created_time = $(this).find("[name='created_time']").text();
        currentStorage.storage_feature = $(this).find("[name='storage_feature']").text();
        currentStorage.price = $(this).find("[name='price']").text();
        currentStorage.notes = $(this).find("[name='notes']").text();
        currentStorage.reporter_name = $(this).find("[name='reporter_name']").text();
        //Set event for the list item
        $("#popupUpdateDelete").popup("open");
    });
}

$(document).on("pagebeforeshow", "#loadpage", function () {
    storageHandler.loadStorages(displayStorages);
});

function deleteStorage() {
    var r = confirm("Storage type: " + currentStorage.storage_type + "\n"
        + "Dimension: " + currentStorage.dimension + "\n"
        + "Price: " + currentStorage.price + "\n");
    if (r == true) {
        storageHandler.deleteStorage(currentStorage.id);
        storageHandler.loadStorages(displayStorages);
    }
    $("#popupUpdateDelete").popup("close");
}

$(document).on("pagebeforeshow", "#updatedialog", function () {
    $("#txtNewStorage_type").val(currentStorage.storage_type);
    $("#txtNewDimension").val(currentStorage.dimension);
    $("#txtNewPrice").val(currentStorage.price);
    $("#txtNewNotes").val(currentStorage.notes);
    $("#txtNewReporter_name").val(currentStorage.reporter_name);
});

function updateStorage() {
    var newStorage_type = $("#txtNewStorage_type").val();
    var newDimension = $("#txtNewDimension").val();
    var newPrice = $("#txtNewPrice").val();
    var newNotes = $("#txtNewNotes").val();
    var newReporter_name = $("#txtNewReporter_name").val();
    storageHandler.updateStorage(currentStorage.id, 
        newDimension, 
        newStorage_type, 
        newPrice, 
        newNotes, 
        newReporter_name);
    $("#updatedialog").dialog("close");
}